#!/usr/bin/env bash

# tomsit: just to build it
mvnw install  -Denforcer.skip -pl core/ -am | tee Build.log~

# tomsit: to run the full test suite
# mvnw install -Denforcer.skip -pl test/ -am | tee Build.log~
